const { Sequelize } = require('sequelize');
const { DATABASE } = require('./db.config')

const sequelize = new Sequelize(
    DATABASE.DATABASE_NAME,
    DATABASE.USERNAME,
    DATABASE.PASSWORD,{
        host: DATABASE.HOST,
        dialect: "mysql"
    }
);

module.exports = sequelize;