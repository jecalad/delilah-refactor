const express = require('express');
const morgan = require('morgan');
const app = express();
require('dotenv').config();

const router = require('../routes/index');

const EXPRESS_PORT = process.env.EXPRESS_PORT || 3000;

app.use(morgan("dev"));

//Body Parse
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded}

app.use('/v1', router);

app.listen(EXPRESS_PORT, ()=>{ 
    console.log(`server listening on port ${EXPRESS_PORT}`);
});