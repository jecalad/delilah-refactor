class UserController {
    constructor (userService){
        this.userService = userService;
    }

    async getUser(req, res){
        const { id } = req.params;
        const user = await this.userService.getUser(id);
        return res.status(200).json(user);
    }

    async createUser(req, res){
        const {firstname, lastname, email, username, password, address, country, phoneNumber } = req.body;
        try{
            const user = await this.userService.createUser(firstname, lastname, email, username, password, address, country, phoneNumber);
            return res.status(200).json(user);
        }catch(err){
            return res.status(500).json(err);
        }
        
    }
}

module.exports = UserController;