const User = require('../models/users.model');
const Profile = require('../models/profile.model');

class UserRepository{
    constructor(){
        this.user = User;
        this.profile = Profile;
        this.user.sync({alter:true});
        this.profile.sync({alter:true});
    }

    async getUser(userId){
        return this.user.findByPk(userId);
    }

    async createUser(firstname, lastname, email, username, password, address, country, phoneNumber){
        const user = {
            nombre: firstname,
            apellido: lastname,
            email: email,
        }
        const profile = {
            nombre_usuario: username,
            contrasena: password,
            direccion: address,
            pais: country,
            telefono: phoneNumber
        }
        let createdUser = await this.user.create(user);
        createdUser.dataValues.perfil = await this.profile.create(profile);
        return createdUser;
    }

}



module.exports = UserRepository;

