const express = require('express');
const router = express.Router();

const UserController = require('../controllers/usuarios.controller');
const UserRepository = require('../repository/users.repository');
const UserService = require('../services/users.service');

const userRepository = new UserRepository();
const userService = new UserService(userRepository);
const userController = new UserController(userService);

router.get('/:id', (req, res)=> userController.getUser(req, res));

router.post('/', (req, res)=> userController.createUser(req, res));

module.exports = router;