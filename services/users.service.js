class UserService {
    constructor(userRepository){
        this.userRepository = userRepository;
    }

    async getUser(userId){
        return this.userRepository.getUser(userId);
    }

    async createUser(firstname, lastname, email, username, password, address, country, phoneNumber){
        return this.userRepository.createUser(firstname, lastname, email, username, password, address, country, phoneNumber);
    }
    
}

module.exports = UserService;