const router = require('express').Router();

const userRoutes = require('./users.routes');


//Endpoint usuarios
router.use('/users', userRoutes);

module.exports = router;